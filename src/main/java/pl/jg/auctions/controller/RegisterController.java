package pl.jg.auctions.controller;

import com.neovisionaries.i18n.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.jg.auctions.domain.dto.AccountDTO;
import pl.jg.auctions.domain.dto.LocationDTO;
import pl.jg.auctions.domain.model.AccountStatus;
import pl.jg.auctions.domain.model.AccountType;
import pl.jg.auctions.domain.model.RoleType;
import pl.jg.auctions.domain.validator.PasswordConfirmationValidator;
import pl.jg.auctions.service.AccountService;
import pl.jg.auctions.service.CountryService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterController extends AbstractController {

    @Autowired
    private CountryService countryService;

    @Autowired
    private PasswordConfirmationValidator passwordConfirmationValidator;

    @Autowired
    private AccountService accountService;

    @ModelAttribute("countries")
    public List<CountryCode> getCountries() {
        return countryService.getCountries();
    }

    @PostConstruct
    private void setUp() {
        title = "Rejestracja użytkownika";
        showUserBox = false;
    }

    @GetMapping("/register")
    public String showForm(Model model) {
        AccountDTO account = new AccountDTO();
        model.addAttribute("account", account);

        LocationDTO location = new LocationDTO();
        model.addAttribute("location", location);

        return "register";
    }

    @PostMapping("/register")
    public String processForm(Model model,
                              @Valid @ModelAttribute("account") AccountDTO account,
                              BindingResult bindingResultAccount,
                              @Valid @ModelAttribute("location") LocationDTO location,
                              BindingResult bindingResultLocation,
                              RedirectAttributes redirectAttributes) {

        passwordConfirmationValidator.validate(account, bindingResultAccount);

        if (bindingResultAccount.hasErrors() || bindingResultLocation.hasErrors()) {

            model.addAttribute("account", account);
            model.addAttribute("location", location);
            model.addAttribute("flashSuccess", false);
            model.addAttribute("flashMessage", "Formularz zawiera błędy");
            return "register";
        }

        accountService.createAccount(
                account,
                location,
                RoleType.ROLE_USER,
                AccountStatus.ACTIVE,
                AccountType.STANDARD);

        // not using Flash object only for "/login" URL (no mutable controller class)
        redirectAttributes.addFlashAttribute("flashMessage", "Rejestracja zakończona pomyślnie. Teraz możesz się zalogować");
        redirectAttributes.addFlashAttribute("flashSuccess", true);
        return "redirect:/login";

    }

}
