package pl.jg.auctions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.jg.auctions.auth.UserDetailsImpl;
import pl.jg.auctions.flash.Flash;
import pl.jg.auctions.persistence.ImageRepository;
import pl.jg.auctions.service.ImageService;

import javax.annotation.PostConstruct;

public abstract class AbstractController {
    @Value("${auctions.sitename}")
    protected String siteName;
    protected String title;
    protected boolean showUserBox;
    @Autowired
    protected Flash flash;

    @ModelAttribute("siteName")
    public String getSiteName() {
        return siteName;
    }

    @ModelAttribute("title")
    public String getTitle() {
        return title;
    }

    @ModelAttribute("showUserBox")
    public boolean getShowUserBox() {
        return showUserBox;
    }

    @ModelAttribute("user")
    public UserDetailsImpl getUser(Model model, Authentication authentication) {
        UserDetailsImpl user = null;
        if (authentication != null && authentication.isAuthenticated()) {
            user = (UserDetailsImpl) authentication.getPrincipal();
        }
        return user;
    }

    @ModelAttribute("flashMessage")
    public String flashMessage() {
        if (flash.hasItems()) {
            return flash.getItem().getMessage();
        }
        return null;
    }

    @ModelAttribute("flashSuccess")
    public boolean flashSuccess() {
        if (flash.hasItems()) {
            return flash.getItem().isSuccess();
        }
        return false;
    }
}
