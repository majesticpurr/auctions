package pl.jg.auctions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.jg.auctions.domain.model.Image;
import pl.jg.auctions.domain.model.ImageResourceType;
import pl.jg.auctions.service.ImageService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ImageController extends AbstractController {

    @Autowired
    ImageService imageService;

    @GetMapping("/img/{resourceType:[A-Z]+}/{resourceId:[0-9]+}/{width:[0-9]+}/{height:[0-9]+}")
    @ResponseBody
    public void getImage(@PathVariable ImageResourceType resourceType,
                         @PathVariable Long resourceId,
                         @PathVariable int width,
                         @PathVariable int height,
                         HttpServletResponse response) throws IOException {

        Image image = imageService.find(resourceType,resourceId,width,height);
        if(image == null) {
            throw new ImageNotFoundException();
        }
        response.setContentType(image.getHttpContentType());
        byte[] content = image.getContent();
        response.setContentLength(content.length);
        response.getOutputStream().write(content);
    }
}
