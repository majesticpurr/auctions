package pl.jg.auctions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.jg.auctions.service.AuctionService;
import pl.jg.auctions.service.CategoryService;

import javax.annotation.PostConstruct;

@Controller
public class HomeController extends AbstractController {
    private static final int AUCTIONS_COUNT_PER_FRAGMENT = 7;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AuctionService auctionService;

    @PostConstruct
    private void setUp() {
        showUserBox = true;
    }

    @GetMapping("/")
    public String home(Model model) {

        model.addAttribute("mainCategories", categoryService.getTopLevelCategories());
        model.addAttribute("mainCategoriesTitle","Kategorie");
        model.addAttribute("newAuctions", auctionService.getLastAddedAuctions(AUCTIONS_COUNT_PER_FRAGMENT));
        model.addAttribute("newAuctionsTitle", "Najnowsze aukcje");
        model.addAttribute("expiringAuctions", auctionService.getExpiringAuctions(AUCTIONS_COUNT_PER_FRAGMENT));
        model.addAttribute("expiringAuctionsTitle", "Aukcje dobiegające końca");

        return "home";
    }
}
