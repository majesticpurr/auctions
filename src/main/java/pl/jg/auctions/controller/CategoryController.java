package pl.jg.auctions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.jg.auctions.domain.model.Category;
import pl.jg.auctions.service.AuctionService;
import pl.jg.auctions.service.CategoryService;

import javax.annotation.PostConstruct;

@Controller
public class CategoryController extends AbstractController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AuctionService auctionService;

    @PostConstruct
    private void setUp() {
        showUserBox = true;
    }

    @GetMapping("/category")
    public String showAll(Model model) {
        model.addAttribute("categories", categoryService.getTopLevelCategories());
        return "categories";
    }

    @GetMapping("/category/show")
    public String show(Model model, @RequestParam long id) {
        Category category = categoryService.getById(id);
        model.addAttribute("category", category);
        return "category";
    }


}
