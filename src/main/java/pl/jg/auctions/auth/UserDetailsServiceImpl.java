package pl.jg.auctions.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.jg.auctions.domain.model.Account;
import pl.jg.auctions.persistence.AccountRepository;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(email);

        if (account != null) {
            UserDetailsImpl userDetailsImpl = new UserDetailsImpl(account);
            return userDetailsImpl;
        } else
            throw new UsernameNotFoundException("no such user: " + email);
    }
}
