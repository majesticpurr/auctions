package pl.jg.auctions.auth;

/*Wrapper class for our domain users - needed to expose security contract
to Spring Security.
This is to decouple/translate domain credentials to obligatory spring credentials
(which are username + password).

*/

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import pl.jg.auctions.domain.model.Account;
import pl.jg.auctions.domain.model.AccountStatus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.stream.Collectors;

public class UserDetailsImpl extends Account implements UserDetails {


    public UserDetailsImpl(Account account) {
        super(account);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //return AuthorityUtils.createAuthorityList("ROLE_USER");
        return AuthorityUtils.createAuthorityList(getRoleNames());
    }


    private String[] getRoleNames() {
        return getRoles().stream()
                .map(r -> r.getType().name())
                .collect(Collectors.toSet())
                .toArray(new String[getRoles().size()]);
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return getStatus() == AccountStatus.ACTIVE;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return getStatus() == AccountStatus.ACTIVE;
    }

}

