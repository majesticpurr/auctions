package pl.jg.auctions.service;

import com.neovisionaries.i18n.CountryCode;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CountryService {
    public List<CountryCode> getCountries() {
        return Arrays.stream(CountryCode.values())
                .sorted(Comparator.comparing(CountryCode::getName))
                .collect(Collectors.toList());
    }
}
