package pl.jg.auctions.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import pl.jg.auctions.domain.model.Auction;
import pl.jg.auctions.persistence.AuctionRepository;

import java.util.List;

@Service
public class AuctionService {

    @Autowired
    private AuctionRepository repository;

    public List<Auction> getLastAddedAuctions(int count) {
        Pageable pageable = PageRequest.of(0, count);
        Slice<Auction> slice = repository.findNew(pageable);
        return slice.getContent();
    }

    public List<Auction> getExpiringAuctions(int count) {
        Pageable pageable = PageRequest.of(0, count);
        Slice<Auction> slice = repository.findExpiring(pageable);
        return slice.getContent();
    }
}
