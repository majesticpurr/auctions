package pl.jg.auctions.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jg.auctions.domain.model.Category;
import pl.jg.auctions.persistence.CategoryRepository;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getTopLevelCategories() {
        return categoryRepository.getAllByParentCategoryIsNullOrderByName();
    }

    public Category getById(Long id) {
        return categoryRepository.getById(id);
    }

}
