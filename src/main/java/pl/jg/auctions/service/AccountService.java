package pl.jg.auctions.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.jg.auctions.domain.dto.AccountDTO;
import pl.jg.auctions.domain.dto.LocationDTO;
import pl.jg.auctions.domain.model.*;
import pl.jg.auctions.persistence.AccountRepository;
import pl.jg.auctions.persistence.RoleRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public void createAccount(AccountDTO accountDTO, LocationDTO locationDTO, RoleType roleType, AccountStatus accountStatus, AccountType accountType) {
        Account account = new Account(accountDTO);
        Location location = new Location(locationDTO);

        account.setPassword(passwordEncoder.encode(account.getPassword()));

        account.setLocation(location);

        Role role = roleRepository.findByType(roleType);
        account.setRoles(Collections.singleton(role));

        account.setStatus(accountStatus);
        account.setType(accountType);

        accountRepository.save(account);
    }
}
