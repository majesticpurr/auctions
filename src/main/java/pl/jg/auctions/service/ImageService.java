package pl.jg.auctions.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.jg.auctions.domain.model.Image;
import pl.jg.auctions.domain.model.ImageResourceType;
import pl.jg.auctions.domain.model.ImageType;
import pl.jg.auctions.image.ImageConverter;
import pl.jg.auctions.image.ImageResizer;
import pl.jg.auctions.persistence.ImageRepository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ImageResizer imageResizer;

    @Autowired
    private ImageConverter imageConverter;

    public void saveDefaultImages() {
        String[] resourceNames = new String[]{"auction", "account", "category"};

        for (String resourceName : resourceNames) {
            Image image = new Image();
            String resourcePath = "/static/img/" + resourceName + "/default.png";
            List<Byte> bytes = new ArrayList<>();
            InputStream inputStream;
            try {
                inputStream = this.getClass().getResource(resourcePath).openStream();
                for (int current = inputStream.read(); current != -1; current = inputStream.read()) {
                    bytes.add((byte) current);
                }
                inputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                continue;
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            image.setResourceType(ImageResourceType.valueOf(resourceName.toUpperCase()));
            image.setOriginal(true);
            image.setDefaultForResourceType(true);
            image.setImageType(ImageType.PNG);

            BufferedImage bufferedImage;

            try {
                inputStream = this.getClass().getResource(resourcePath).openStream();
                bufferedImage = imageConverter.convertInputStreamToBufferedImage(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            image.setWidth(bufferedImage.getWidth());
            image.setHeight(bufferedImage.getHeight());

            byte[] bytesArray = new byte[bytes.size()];
            for (int i = 0; i < bytes.size(); i++) {
                bytesArray[i] = bytes.get(i).byteValue();
            }

            image.setContent(bytesArray);
            imageRepository.save(image);
        }

    }

    @Transactional
    public Image find(ImageResourceType resourceType, Long resourceId, int width, int height) throws IOException {

        Image image = imageRepository.findFirstByResourceTypeAndResourceIdAndWidthAndHeight(resourceType, resourceId, width, height);

        if (image != null) {
            return image;
        }

        image = findOriginal(resourceType, resourceId);

        if (image == null) {
            image = findDefault(resourceType, width, height);
            if (image != null) {
                return image;
            }
            image = findDefaultOriginal(resourceType);
            if (image == null) {
                return null;
            }
        }

        image = imageResizer.resize(image, width, height);

        imageRepository.save(image);

        return image;
    }

    private Image findOriginal(ImageResourceType resourceType, Long resourceId) {
        return imageRepository.findFirstByResourceTypeAndResourceIdAndOriginalIsTrue(resourceType, resourceId);
    }

    private Image findDefault(ImageResourceType resourceType, int width, int height) {
        return imageRepository.findFirstByResourceTypeAndWidthAndHeightAndDefaultForResourceTypeIsTrue(resourceType, width, height);
    }

    private Image findDefaultOriginal(ImageResourceType resourceType) {
        return imageRepository.findFirstByResourceTypeAndOriginalIsTrueAndDefaultForResourceTypeIsTrue(resourceType);
    }


}
