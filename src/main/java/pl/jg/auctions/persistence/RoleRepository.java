package pl.jg.auctions.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jg.auctions.domain.model.Role;
import pl.jg.auctions.domain.model.RoleType;

@Repository
public interface RoleRepository extends CrudRepository<Role,Long> {
    public Role findByType(RoleType type);
}
