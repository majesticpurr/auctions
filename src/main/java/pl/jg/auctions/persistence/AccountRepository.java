package pl.jg.auctions.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jg.auctions.domain.model.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account,Long> {

    public Account findByEmail(String email);

}
