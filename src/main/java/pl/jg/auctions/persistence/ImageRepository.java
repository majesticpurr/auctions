package pl.jg.auctions.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jg.auctions.domain.model.Image;
import pl.jg.auctions.domain.model.ImageResourceType;

@Repository
public interface ImageRepository extends CrudRepository<Image, Long> {

    Image findFirstByResourceTypeAndResourceIdAndWidthAndHeight(ImageResourceType resourceType,
                                                                Long resourceId,
                                                                int with,
                                                                int height);

    Image findFirstByResourceTypeAndResourceIdAndOriginalIsTrue(ImageResourceType resourceType,
                                                                Long resourceId);

    Image findFirstByResourceTypeAndWidthAndHeightAndDefaultForResourceTypeIsTrue(ImageResourceType resourceType,
                                                                                  int width,
                                                                                  int height);

    Image findFirstByResourceTypeAndOriginalIsTrueAndDefaultForResourceTypeIsTrue(ImageResourceType resourceType);


}
