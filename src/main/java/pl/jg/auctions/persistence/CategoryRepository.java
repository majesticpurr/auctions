package pl.jg.auctions.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jg.auctions.domain.model.Category;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category,Long> {
    List<Category> getAllByParentCategoryIsNullOrderByName();
    Category getById(Long id);
}
