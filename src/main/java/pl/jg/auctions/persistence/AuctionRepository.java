package pl.jg.auctions.persistence;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.jg.auctions.domain.model.Auction;

import java.util.List;

@Repository
public interface AuctionRepository extends CrudRepository<Auction,List> {

    @Query("select a from Auction a where a.isActive = true and a.expiresAt >= current_timestamp order by a.id desc")
    Slice<Auction> findNew(Pageable pageable);

    @Query("select a from Auction a where a.isActive = true and a.expiresAt >= current_timestamp order by a.expiresAt")
    Slice<Auction> findExpiring(Pageable pageable);
}
