package pl.jg.auctions.domain.dto;

import com.neovisionaries.i18n.CountryCode;
import pl.jg.auctions.domain.model.Location;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LocationDTO {
    private Long id;
    @NotBlank
    private String state;
    @NotBlank
    private String town;
    @NotBlank
    private String street;
    @NotBlank
    private String buildingNr;
    private String apartmentNr;
    @NotBlank
    private String postalCode;

    @NotNull
    private CountryCode country;

    public LocationDTO() {
    }

    public LocationDTO(Location location) {
        if (location != null) {
            this.id = location.getId();
            this.state = location.getState();
            this.town = location.getTown();
            this.street = location.getStreet();
            this.buildingNr = location.getBuildingNr();
            this.apartmentNr = location.getApartmentNr();
            this.postalCode = location.getPostalCode();
            this.country = location.getCountry();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNr() {
        return buildingNr;
    }

    public void setBuildingNr(String buildingNr) {
        this.buildingNr = buildingNr;
    }

    public String getApartmentNr() {
        return apartmentNr;
    }

    public void setApartmentNr(String apartmentNr) {
        this.apartmentNr = apartmentNr;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }
}
