package pl.jg.auctions.domain.dto;

import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.Role;
import pl.jg.auctions.domain.model.RoleType;

public class RoleDTO {
    private Long id;
    private RoleType type;

    public RoleDTO() {}

    public RoleDTO(Role role) {
        this.id = role.getId();
        this.type = role.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleType getType() {
        return type;
    }

    public void setType(RoleType type) {
        this.type = type;
    }
}
