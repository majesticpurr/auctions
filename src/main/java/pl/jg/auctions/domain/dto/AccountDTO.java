package pl.jg.auctions.domain.dto;

import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class AccountDTO {

    private Long id;

    @Email
    @NotBlank
    private String email;
    @NotBlank
    private String nick;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private LocalDateTime createdAt;

    @Size(min = 6, message = "Hasło powinno mieć przynajmniej 6 znaków")
    private String password;
    private String passwordConfirm;
    private AccountType type;
    private AccountStatus status;
    private LocationDTO location = new LocationDTO();
    private Set<RoleDTO> roles = new HashSet<>();


    public AccountDTO() {}

    public AccountDTO(Account account) {
        if (account != null) {
            this.id = account.getId();
            this.email = account.getEmail();
            this.nick = account.getNick();
            this.firstName = account.getFirstName();
            this.lastName = account.getLastName();
            this.createdAt = account.getCreatedAt();
            this.password = account.getPassword();
            this.type = account.getType();
            this.status = account.getStatus();

            this.location = new LocationDTO(account.getLocation());

            Set<Role> accountRoles = account.getRoles();

            if (accountRoles != null) {
                this.roles = accountRoles.stream()
                        .map(RoleDTO::new)
                        .collect(Collectors.toSet());
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDTO> roles) {
        this.roles = roles;
    }
}
