package pl.jg.auctions.domain.model;

import com.neovisionaries.i18n.CountryCode;
import pl.jg.auctions.domain.dto.LocationDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(indexes = {
        @Index(columnList = "country, state, town, street", name = "country_state_town_street_idx"),
        @Index(columnList = "country, postal_code", name = "country_postal_code_idx")
})
public class Location implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private String town;

    private String street;

    @Column(name = "building_nr")
    private String buildingNr;

    @Column(name = "apartment_nr")
    private String apartmentNr;

    @Column(name = "postal_code")
    private String postalCode;

    @Enumerated(EnumType.STRING)
    private CountryCode country;


    public Location() {
    }

    public Location(LocationDTO locationDTO) {
        //this.id = locationDTO.getId();
        this.state = locationDTO.getState();
        this.town = locationDTO.getTown();
        this.street = locationDTO.getStreet();
        this.buildingNr = locationDTO.getBuildingNr();
        this.apartmentNr = locationDTO.getApartmentNr();
        this.postalCode = locationDTO.getPostalCode();
        this.country = locationDTO.getCountry();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNr() {
        return buildingNr;
    }

    public void setBuildingNr(String buildingNr) {
        this.buildingNr = buildingNr;
    }

    public String getApartmentNr() {
        return apartmentNr;
    }

    public void setApartmentNr(String apartmentNr) {
        this.apartmentNr = apartmentNr;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }
}
