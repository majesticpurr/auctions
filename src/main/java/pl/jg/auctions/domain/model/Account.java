package pl.jg.auctions.domain.model;

import org.hibernate.annotations.NaturalId;
import org.springframework.beans.factory.annotation.Autowired;
import pl.jg.auctions.domain.dto.AccountDTO;
import pl.jg.auctions.image.ImageUrlBuilder;

import javax.persistence.*;
import java.io.Serializable;
/*import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;*/
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Account implements Serializable, ImageHolder {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    @NaturalId
    private String email;

    @NaturalId
    @Column(nullable = false, unique = false)
    private String nick;


    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true)
    @JoinColumn(name = "location_id")
    private Location location;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;


    @PrePersist
    protected void onCreate() {
        createdAt = LocalDateTime.now();
    }

    @Enumerated
    @Column(nullable = false)
    private AccountType type = AccountType.STANDARD;

    @Enumerated
    @Column(nullable = false)
    private AccountStatus status = AccountStatus.ACTIVE;


    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true, mappedBy = "owner", fetch = FetchType.LAZY)
    transient private Set<Auction> auctions = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();

    private String password;

    @Transient
    @Autowired
    private ImageUrlBuilder imageUrlBuilder;

    public Account() {
    }

    public Account(Account account) {

        this.id = account.getId();
        this.email = account.getEmail();
        this.nick = account.getNick();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.location = account.getLocation();
        this.createdAt = account.getCreatedAt();
        this.type = account.getType();
        this.status = account.getStatus();
        this.auctions = account.getAuctions();
        this.roles = account.getRoles();
        this.password = account.getPassword();

    }

    public Account(AccountDTO accountDTO) {

        //this.id = accountDTO.getId();
        this.email = accountDTO.getEmail();
        this.nick = accountDTO.getNick();
        this.firstName = accountDTO.getFirstName();
        this.lastName = accountDTO.getLastName();
        this.location = new Location(accountDTO.getLocation());
        this.type = accountDTO.getType();
        this.status = accountDTO.getStatus();
        //this.auctions = accountDTO.getAuctions();
        this.roles = accountDTO.getRoles().stream()
                .map(Role::new)
                .collect(Collectors.toSet());
        this.password = accountDTO.getPassword();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public Set<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(Set<Auction> auctions) {
        this.auctions = auctions;
    }

    public void addAuction(Auction auction) {
        auctions.add(auction);
        auction.setOwner(this);
    }

    public void removeAuction(Auction auction) {
        auctions.remove(auction);
        auction.setOwner(null);
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        roles.add(role);
        role.addAccount(this);
    }

    public void removeRole(Role role) {
        roles.remove(role);
        role.removeAccount(this);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public ImageResourceType getResourceType() {
        return ImageResourceType.ACCOUNT;
    }

    @Override
    public Long getResourceId() {
        return getId();
    }

    @Override
    public String getImageUrl(int width, int height) {
        return imageUrlBuilder.build(this, width, height);
    }
}