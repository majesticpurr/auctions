package pl.jg.auctions.domain.model;

import javax.persistence.*;

@Entity
@Table(indexes = {
        @Index(columnList = "resource_type,resource_id,width,height", unique = true),
        @Index(columnList = "resource_type,resource_id,is_original"),
        @Index(columnList = "resource_type,is_default")
})
public class Image {

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "resource_type")
    private ImageResourceType resourceType;

    @Column(name = "resource_id")
    private Long resourceId;

    @Column(name = "is_original")
    private boolean original;

    @Enumerated(EnumType.STRING)
    @Column(name = "image_type", nullable = false)
    private ImageType imageType;

    @Column(nullable = false)
    private int width;

    @Column(nullable = false)
    private int height;

    @Lob()
    @Column(nullable = false)
    private byte[] content;

    @Column(name="is_default")
    private boolean defaultForResourceType;

    public Image() {}

    public Image(Image image) {
        this.resourceType = image.getResourceType();
        this.resourceId = image.getResourceId();
        this.original = image.getOriginal();
        this.imageType = image.getImageType();
        this.width = image.getWidth();
        this.height=image.getHeight();
        this.content = image.getContent();
        this.defaultForResourceType = image.getDefaultForResourceType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ImageResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ImageResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public boolean getOriginal() {
        return original;
    }

    public void setOriginal(boolean original) {
        this.original = original;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public boolean getDefaultForResourceType() {
        return defaultForResourceType;
    }

    public void setDefaultForResourceType(boolean defaultForResourceType) {
        this.defaultForResourceType = defaultForResourceType;
    }

    public String getHttpContentType() {
        return imageType.getMimeType();
    }

}
