package pl.jg.auctions.domain.model;

public interface ImageHolder {
    ImageResourceType getResourceType();

    Long getResourceId();

    String getImageUrl(int width, int height);
}
