package pl.jg.auctions.domain.model;

import org.springframework.http.MediaType;

import java.util.stream.Stream;

public enum ImageType {
    JPG(MediaType.IMAGE_JPEG_VALUE),
    PNG(MediaType.IMAGE_PNG_VALUE),
    GIF(MediaType.IMAGE_GIF_VALUE);

    private String mimeType;

    ImageType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getMimeType() {
        return mimeType;
    }

    public static ImageType findByMimeType(String mimeType) {
        return Stream.of(values())
                .filter(imageType -> imageType.getMimeType().equals(mimeType))
                .findFirst()
                .orElse(null);
    }

    public String getImageIOFormatName() {
        return name().toLowerCase();
    }

}
