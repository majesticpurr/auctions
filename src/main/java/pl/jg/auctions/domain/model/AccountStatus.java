package pl.jg.auctions.domain.model;

public enum AccountStatus {
    ACTIVE,
    INACTIVE,
    DISABLED;
}
