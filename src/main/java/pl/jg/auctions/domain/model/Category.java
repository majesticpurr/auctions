package pl.jg.auctions.domain.model;

import org.springframework.beans.factory.annotation.Autowired;
import pl.jg.auctions.image.ImageUrlBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Category implements Serializable, ImageHolder {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(length = 1024)
    private String description;

    @Lob
    private byte[] logo;

    @OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Category> childCategories;

    @ManyToOne
    Category parentCategory;

    @OneToMany(mappedBy = "category")
    List<Auction> auctions = new ArrayList<>();

    @Transient
    @Autowired
    private ImageUrlBuilder imageUrlBuilder;

    public Category() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public List<Category> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<Category> childCategories) {
        this.childCategories = childCategories;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public void addChildCategory(Category childCategory) {
        childCategory.setParentCategory(this);
        childCategories.add(childCategory);
    }

    public void removeChildCategory(Category childCategory) {
        childCategory.setParentCategory(null);
        childCategories.remove(childCategory);
    }

    @Override
    public ImageResourceType getResourceType() {
        return ImageResourceType.CATEGORY;
    }

    @Override
    public Long getResourceId() {
        return getId();
    }

    @Override
    public String getImageUrl(int width, int height) {
        return imageUrlBuilder.build(this, width, height);
    }
}
