package pl.jg.auctions.domain.model;

public enum ImageSize {
    SMALL,
    BIG,
    ORIGINAL;

    public String getSubDirName() {
        return name().toLowerCase();
    }
}
