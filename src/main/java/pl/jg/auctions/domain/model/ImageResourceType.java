package pl.jg.auctions.domain.model;

import java.util.stream.Stream;

public enum ImageResourceType {
    CATEGORY("/img/category/", Category.class),
    AUCTION("/img/auction/", Auction.class),
    ACCOUNT("/img/account/", Account.class);

    private String path;
    private Class resourceClass;

    ImageResourceType(String path, Class resourceClass) {
        this.path = path;
        this.resourceClass = resourceClass;
    }

    public String getPath() {
        return path;
    }

    public Class getResourceClass() {
        return resourceClass;
    }

    public static ImageResourceType getByResourceClass(Class resourceClass) {
        return Stream.of(ImageResourceType.values())
                .filter(imageResourceType -> imageResourceType.getResourceClass().equals(resourceClass))
                .findFirst()
                .orElseGet(null);
    }


}
