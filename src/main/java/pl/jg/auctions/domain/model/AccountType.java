package pl.jg.auctions.domain.model;

public enum AccountType {
    STANDARD,
    PREMIUM;
}
