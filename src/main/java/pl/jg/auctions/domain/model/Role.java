package pl.jg.auctions.domain.model;

import org.hibernate.annotations.NaturalId;
import pl.jg.auctions.domain.dto.RoleDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Role implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false)
    @NaturalId
    private RoleType type;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    public Role() {
    }

    public Role(RoleDTO roleDTO) {
        this.id = roleDTO.getId();
        this.type = roleDTO.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleType getType() {
        return type;
    }

    public void setType(RoleType type) {
        this.type = type;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(Account account) {
        accounts.add(account);
        account.addRole(this);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
        account.removeRole(this);
    }

}
