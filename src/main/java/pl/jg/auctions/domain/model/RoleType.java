package pl.jg.auctions.domain.model;

public enum RoleType {
    ROLE_USER,
    ROLE_ADMIN;
}
