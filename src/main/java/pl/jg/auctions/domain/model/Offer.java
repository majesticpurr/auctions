package pl.jg.auctions.domain.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Offer {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "auction_id",
            foreignKey = @ForeignKey(name = "auction_id_fk")
    )
    private Auction auction;

    @ManyToOne
    @JoinColumn(name = "account_id",
            foreignKey = @ForeignKey(name = "account_id_fk"))
    private Account account;


    private BigDecimal price;

    public Offer() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
