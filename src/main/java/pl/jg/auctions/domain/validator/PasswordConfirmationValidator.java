package pl.jg.auctions.domain.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.jg.auctions.domain.dto.AccountDTO;


@Component
public class PasswordConfirmationValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(AccountDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","validation.anyfield.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"passwordConfirm","validation.anyfield.empty");
        AccountDTO accountDTO = (AccountDTO) o;
        if(! accountDTO.getPassword().equals(accountDTO.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm","validation.password.notequal");
        }
    }
}
