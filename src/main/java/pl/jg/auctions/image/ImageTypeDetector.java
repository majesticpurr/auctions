package pl.jg.auctions.image;

import pl.jg.auctions.domain.model.ImageType;

import java.io.IOException;

public interface ImageTypeDetector {

    ImageType detectImageType(byte[] fileContent) throws IOException;
}
