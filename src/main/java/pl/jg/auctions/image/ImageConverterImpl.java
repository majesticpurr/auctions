package pl.jg.auctions.image;

import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.ImageType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.*;

@Component
public class ImageConverterImpl implements ImageConverter {

    public int[] convertByteArray2Pixels(byte[] fileContent) throws IOException {
        InputStream inputStream = convertByteArrayToInputStream(fileContent);
        BufferedImage bufferedImage = convertInputStreamToBufferedImage(inputStream);
        inputStream.close();
        return convertBufferedImageToPixels(bufferedImage);
    }

    @Override
    public ImageProcessor convertByteArrayToImageProcessor(byte[] fileContent) throws IOException {
        InputStream inputStream = convertByteArrayToInputStream(fileContent);
        BufferedImage bufferedImage = convertInputStreamToBufferedImage(inputStream);
        return new ImagePlus("image", bufferedImage).getProcessor();
    }

    @Override
    public byte[] convertImageProcessorToByteArray(ImageProcessor imageProcessor, ImageType imageType) throws IOException {
        BufferedImage bufferedImage = imageProcessor.getBufferedImage();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String formatName = imageType.getImageIOFormatName();
        ImageIO.write(bufferedImage, formatName, outputStream);
        byte[] result = outputStream.toByteArray();
        outputStream.close();
        return result;
    }

    @Override
    public InputStream convertByteArrayToInputStream(byte[] fileContent) {
        return new ByteArrayInputStream(fileContent);
    }

    @Override
    public BufferedImage convertInputStreamToBufferedImage(InputStream inputStream) throws IOException {
        return ImageIO.read(inputStream);
    }

    public int[] convertBufferedImageToPixels(BufferedImage bufferedImage) {
        Raster raster = bufferedImage.getData();
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        int[] pixels = new int[width * height];
        return raster.getPixels(0, 0, width, height, pixels);
    }


}
