package pl.jg.auctions.image;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.ImageType;

import java.io.IOException;
import java.io.InputStream;

@Component
public class ImageTypeDetectorImpl implements ImageTypeDetector {

    @Autowired
    private ImageConverter imageConverter;

    @Override
    public ImageType detectImageType(byte[] fileContent) throws IOException {
        Tika tika = new Tika();
        InputStream inputStream = imageConverter.convertByteArrayToInputStream(fileContent);
        String type = tika.detect(inputStream);
        inputStream.close();
        return ImageType.findByMimeType(type);
    }
}
