package pl.jg.auctions.image;

import ij.process.ImageProcessor;
import pl.jg.auctions.domain.model.ImageType;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public interface ImageConverter {
    ImageProcessor convertByteArrayToImageProcessor(byte[] fileContent) throws IOException;

    byte[] convertImageProcessorToByteArray(ImageProcessor imageProcessor, ImageType imageType) throws IOException;

    InputStream convertByteArrayToInputStream(byte[] fileContent);

    BufferedImage convertInputStreamToBufferedImage(InputStream inputStream) throws IOException;
}
