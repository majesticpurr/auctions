package pl.jg.auctions.image;

import ij.process.ImageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.Image;
import pl.jg.auctions.domain.model.ImageType;

import java.io.IOException;

@Component
public class ImageResizer {

    @Value("${auctions.image.min-width}")
    private int minWidth;


    @Value("${auctions.image.min-height}")
    private int minHeight;

    @Autowired
    private ImageConverter imageConverter;

    public Image resize(Image srcImage, int dstWidth, int dstHeight) throws IOException {

        int srcWidth = srcImage.getWidth();
        int srcHeight = srcImage.getHeight();
        byte[] srcContent = srcImage.getContent();
        ImageType srcImageType = srcImage.getImageType();

        ImageProcessor imageProcessor = scale(srcWidth, srcHeight, dstWidth, dstHeight, srcContent);

        imageProcessor = crop(imageProcessor, dstWidth, dstHeight);

        Image resultImage = new Image(srcImage);
        resultImage.setWidth(dstWidth);
        resultImage.setHeight(dstHeight);

        byte[] dstContent = imageConverter.convertImageProcessorToByteArray(imageProcessor, srcImageType);
        resultImage.setContent(dstContent);

        return resultImage;
    }

    private double getRatio(int size1, int size2) {
        double dSize1 = (double) size1;
        return dSize1 / size2;
    }

    private ImageProcessor scale(int srcWidth, int srcHeight, int dstWidth, int dstHeight, byte[] srcContent) throws IOException {

        double hScale = getRatio(dstWidth, srcWidth);
        double vScale = getRatio(dstHeight, srcHeight);

        ImageProcessor imageProcessor = imageConverter.convertByteArrayToImageProcessor(srcContent);

        boolean wasRotated = false;
        int widthAfterScaling = dstWidth;

        if (srcWidth != dstWidth || srcHeight != dstHeight) {
            if (vScale > hScale) {
                imageProcessor = imageProcessor.rotateRight();
                wasRotated = true;
                widthAfterScaling = dstHeight;
            }

            imageProcessor = imageProcessor.resize(widthAfterScaling);

            if (wasRotated) {
                imageProcessor = imageProcessor.rotateLeft();
            }
        }

        return imageProcessor;
    }

    private ImageProcessor crop(ImageProcessor imageProcessor, int dstWidth, int dstHeight) {

        ImageProcessor resultImageProcessor = imageProcessor;

        int width = imageProcessor.getWidth();
        int height = imageProcessor.getHeight();

        if (width != dstWidth) {
            int cropX = (width - dstWidth) / 2;
            imageProcessor.setRoi(cropX, 0, dstWidth, dstHeight);
            resultImageProcessor = imageProcessor.crop();
        } else if (height != dstHeight) {
            int cropY = (height - dstHeight) / 2;
            imageProcessor.setRoi(0, cropY, dstWidth, dstHeight);
            resultImageProcessor = imageProcessor.crop();
        }

        return resultImageProcessor;
    }

}
