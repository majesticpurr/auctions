package pl.jg.auctions.image;

import org.springframework.stereotype.Component;
import pl.jg.auctions.domain.model.ImageHolder;

@Component
public class ImageUrlBuilder {

    public String build(ImageHolder imageHolder, int width, int height) {
        return "/img/" + imageHolder.getResourceType().toString() + "/" + imageHolder.getResourceId() + "/" + width + "/" + height;
    }

}
