package pl.jg.auctions.flash;

import java.io.Serializable;

public class FlashItem implements Serializable {
    private String message;
    private boolean success;

    public FlashItem(String message) {
        this(message,true);
    }

    public FlashItem(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
