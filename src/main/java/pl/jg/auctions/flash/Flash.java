package pl.jg.auctions.flash;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;

public class Flash implements Serializable {
    private Deque<FlashItem> items = new ArrayDeque<>();

    public void addMessage(FlashItem flashItem) {
        items.offerFirst(flashItem);
    }

    public void addMessage(String message, boolean success) {
        addMessage(new FlashItem(message, success));
    }

    public void addMessage(String message) {
        addMessage(new FlashItem(message));
    }

    public FlashItem getItem() {
        return items.pollLast();
    }

    public Deque<FlashItem> getItems() {
        Deque<FlashItem> items = new ArrayDeque<>(this.items);
        this.items = new ArrayDeque<>();
        return items;
    }

    public boolean hasItems() {
        return ! items.isEmpty();
    }
}
