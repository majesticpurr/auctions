package pl.jg.auctions.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jg.auctions.service.ImageService;

import javax.annotation.PostConstruct;

@Component
public class DefaultImagesLoader {

    @Autowired
    ImageService imageService;


    @PostConstruct
    public void storeDefaultImages() {
        imageService.saveDefaultImages();
    }
}
